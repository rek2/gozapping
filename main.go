/* Fast GO API example to show the power of zaproxy */
/* Made this in 30m to show other people from hispagatos */
/* ReK2 */

package main

import (
	"crypto/tls"
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"time"

	"github.com/zaproxy/zap-api-go/zap"
)

var target string

func init() {
	flag.StringVar(&target, "target", "https://hispagatos.space", "target address")
	flag.Parse()
}

func writeReport(target, report string) {
	regex := regexp.MustCompile(`^(http://|https://)`)
	target = regex.ReplaceAllString(target, "report-")
	filename := fmt.Sprintf("%s.html", target)

	f, err := os.Create(filename)
	if err != nil {
		fmt.Println(err)
		return
	}
	l, err := f.WriteString(report)
	if err != nil {
		fmt.Println(err)
		f.Close()
		return

	}
	fmt.Printf("%v bytes written succesfully to %s\n", l, target)
	err = f.Close()
	if err != nil {
		fmt.Println(err)
		return
	}
}

func main() {

	token := os.Getenv("ZAPTOKEN")

	cfg := &zap.Config{
		Base:      zap.DefaultHTTPSBase,
		BaseOther: zap.DefaultHTTPSBaseOther,
		APIKey:    token,
		TLSConfig: tls.Config{
			InsecureSkipVerify: true,
		},

		Proxy: "http://127.0.0.1:8081",
	}
	client, err := zap.NewClient(cfg)
	if err != nil {
		log.Fatal(err)
	}

	// Spider target
	fmt.Println("Spider : " + target)
	resp, err := client.Spider().Scan(target, "", "", "", "")
	if err != nil {
		log.Fatal(err)
	}
	// The scan now returns a scan id to support concurrent scanning
	scanid := resp["scan"].(string)
	for {
		time.Sleep(1000 * time.Millisecond)
		resp, _ = client.Spider().Status(scanid)
		progress, _ := strconv.Atoi(resp["status"].(string))
		if progress >= 100 {
			break
		}
	}
	fmt.Println("Spider complete")

	// Give the passive scanner a chance to complete
	time.Sleep(2000 * time.Millisecond)

	fmt.Println("Active scan : " + target)
	resp, err = client.Ascan().Scan(target, "True", "False", "", "", "", "")
	if err != nil {
		log.Fatal(err)
	}
	// The scan now returns a scan id to support concurrent scanning
	scanid = resp["scan"].(string)
	for {
		time.Sleep(5000 * time.Millisecond)
		resp, _ = client.Ascan().Status(scanid)
		progress, _ := strconv.Atoi(resp["status"].(string))
		fmt.Printf("Active Scan progress : %d\n", progress)
		if progress >= 100 {
			break
		}
	}
	fmt.Println("Active Scan complete")
	report, err := client.Core().Htmlreport()
	if err != nil {
		log.Fatal(err)
	}
	writeReport(target, string(report))
}
