# gozapping

== Example using Zaproxy API with GO ==
please RTFM:  
[read the Zap API](https://www.zaproxy.org/docs/api/)


1. Set token
```

export ZAPTOKEN=yourzaptoken
```

2. Run Zaproxy on daemon mode:

```


#!/bin/sh
# name this script zap.sh

cd /usr/share/zaproxy
./zap.sh -daemon -config api.key=yourzaptoken

```
```
chmod 755 ./zap.sh
./zap.sh
```


3. Run program
```
./gozappin -target https://matrix.hispagatos.org
Spider : https://matrix.hispagatos.org
Spider complete
Active scan : https://matrix.hispagatos.org
Active Scan progress : 81
Active Scan progress : 100
Active Scan complete
Alerts:
```
